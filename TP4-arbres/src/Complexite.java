public class Complexite {

    public int estMajoritaire(int []t) {
        for (int i = 0; i < t.length; i++) {
            int nb = 0;
            for (int j = 0; j < t.length; j++) {
                if (t[i] == t[j]) {
                    nb++;
                }
            }
            if (nb > t.length / 2) {
                return t[i];
            }
        }
        return -1;
    }

    public boolean estMajoritaireIndice(int []t, int x, int i, int j) {
        int cpt = 0;
        for (int indice = i; indice < j; indice++) {
            if (t[indice] == x){
                cpt++;
            }
        }
        return cpt > (j - i + 1) / 2;
    }

    /*public int elementMajoritaireAux(int[]t, int i, int j) {


        if (t.length == 0) {
            return -1;
        }
        else if (t.length == 1) {
            return t[0];
        }
        else {
            int x1 = elementMajoritaireAux(t, i, (j - i + 1) / 2);
            int x2 = elementMajoritaireAux(t, (j - i + 1) / 2, j);

            if (estMajoritaireIndice(t, x1, i, j)) {
                return x1;
            } else if (estMajoritaireIndice(t, x2, i, j)) {
                return x2;
            }
        }
        return -1;
    }*/
    public int elementMajoritaireAux(int[] t, int i, int j) {
        if (i == j) {
            return t[i];
        }

        int x = elementMajoritaireAux(t, i, (i + j) / 2);
        int y = elementMajoritaireAux(t, (i + j) / 2 + 1, j);

        if (x == y) {
            return x;
        }

        if (estMajoritaireIndice(t, x, i, j)) {
            return x;
        } else if (estMajoritaireIndice(t, y, i, j)) {
            return y;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        int[] tableau = {1, 2, 6, 5, 8, 8, 8, 8};
        Complexite c = new Complexite();
        //System.out.println(c.estMajoritaire(tableau));

        // j < t.lenght
        System.out.println(c.estMajoritaireIndice(tableau, 8, 4, tableau.length));

        System.out.println(c.elementMajoritaireAux(tableau, 0, tableau.length - 1));
    }
}
