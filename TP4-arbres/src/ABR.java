import java.util.ArrayList;
import java.util.List;
import java.util.Objects;




public class ABR{
    private int val;
    private ABR filsG;
    private ABR filsD;
    //fisG == null <=> filsD == null

    //on supposera en prérequis sous-entendu que this est un ABR dans toutes les méthodes (sauf mentionné explicitement,
    //pour  la méthode verifie)
    ///////////////////////////////////////////////////////
    ////// méthodes fournies
    ///////////////////////////////////////////////////////

    public ABR(){
        filsG = null;
        filsD=  null;
    }

    public ABR(int x, ABR g, ABR d){
        val = x;
        filsG = g;
        filsD=  d;
    }

    public ABR(Liste l) {
        for (int i = 0; i < l.longeur(); i++) {
            this.insert(l.getVal());
            l = l.getSuiv();
        }
    }

    public int getVal() {
        //sur arbre non vide
        if(estVide())
            throw new RuntimeException("getVal appelée sur ABR vide");
        return val;
    }


    public boolean estVide(){
        return filsG==null;
    }

    public boolean egal(ABR a){
        if(estVide() != a.estVide())
            return false;
        if(estVide())
            return true;
        //les 2 non vides
        return getVal()==a.getVal() && filsG.egal(a.filsG) && filsD.egal(a.filsD);

    }


    public String toStringV2aux(String s){
        //pre : aucun
        //resultat : retourne une chaine de caracteres permettant d'afficher this dans un terminal (avec l'indentation du dessin precedent, et en ajoutant s au debut de chaque ligne ecrite) et passe a la ligne

        if( estVide ())
            return s+"()\n";
        else
            return filsD.toStringV2aux (s + "     ") + s + getVal() + "\n"+ filsG.toStringV2aux (s + "     ");
    }

    public String toString(){
        return toStringV2aux("");
    }





    ///////////////////////////////////////////////////////
    ////// méthodes demandées dans le TP
    ///////////////////////////////////////////////////////

    public boolean recherche(int x){
        if (estVide()) {
            return false;
        }
        else {
            return getVal() == x || filsG.recherche(x) || filsD.recherche(x);
        }
    }
    public void insert(int x){
        if (estVide()) {
            val = x;
            filsG = new ABR();
            filsD = new ABR();
        }
        else {
            if (x <= getVal()) {
                filsG.insert(x);
            }
            else {
                filsD.insert(x);
            }
        }
    }

    public int max(){
        //retourne l'entier max de this (et -infini si vide)
        if (this.estVide()) {
            return Integer.MIN_VALUE;
        }
        else{
            return Math.max(val, Math.max(filsG.max(), filsD.max()));
        }
    }


    public void suppr(int x) {
        //supprime x de this, et ne fait rien si x n'est pas présent
        if (!recherche(x)) {
            return;
        }
        else if (x == val) {
            if (filsG.estVide() && filsD.estVide()) {
                val = Integer.MIN_VALUE;
            }
            else if (filsG.estVide()) {
                val = filsD.val;
                filsG = filsD.filsG;
                filsD = filsD.filsD;
            }
            else if (filsD.estVide()) {
                val = filsG.val;
                filsD = filsG.filsD;
                filsG = filsG.filsG;
            }
            else {
                val = filsG.max();
                filsG.suppr(val);
            }
        }
        else if (x < val) {
            filsG.suppr(x);
        }
        else {
            filsD.suppr(x);
        }
    }


    public boolean estEgal(ABR a){
        //retourne vrai ssi l'arbre this est égal à a (ce qui signifie possède les mêmes valeurs, rangées aux mêmes endroits)
        if (this.estVide() && a.estVide()) {
            return true;
        }
        return a.val == this.val && filsG.estEgal(a.filsG) && filsD.estEgal(a.filsD);
    }

    public void toListeTrieeV2aux(Liste l) {
        //Ecrire une méthode Liste toListeTriee() qui retourne dans une liste toutes les valeurs de l’arbre, triées par
        //ordre croissant. ez utilisé s’appelle le parcours infixe.

        if (!this.estVide()) {
            filsD.toListeTrieeV2aux(l);
            l.ajoutTete(val);
            filsG.toListeTrieeV2aux(l);
        }
    }
    public Liste toListeTrierV2() {
        Liste liste = new Liste();
        this.toListeTrieeV2aux(liste);
        return liste;
    }
    public boolean verifieNaive() {
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        if (this.estVide()) {
            return true;
        }
        else {
            return filsG.max() <= val && val <= filsD.min() && filsG.verifieNaive() && filsD.verifieNaive();
        }
    }

    private int min() {
        if (this.estVide()) {
            return Integer.MAX_VALUE;
        }
        else {
            return Math.min(val, Math.min(filsG.min(), filsD.min()));
        }
    }

    public boolean verifieV1(){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        //appel à verifABRV1

        if (this.estVide()) {
            return true;
        }
        else {
            return filsG.verifABRV1(Integer.MIN_VALUE, val) && filsD.verifABRV1(val, Integer.MAX_VALUE);
        }

    }

    public boolean verifABRV1(int m, int M){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR


        if (this.estVide()) {
            return true;
        }
        else {
            return m <= val && val <= M && filsG.verifABRV1(m, val) && filsD.verifABRV1(val, M);
        }
    }

    public boolean verifieV2() {
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        //appel à verifABRV2
        throw new RuntimeException("méthode non implémentée");
    }

    public int[] verifABRV2(){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        throw new RuntimeException("méthode non implémentée");
    }




    ///////////////////////////////////////////////////////
    ////// méthodes utiles seulement pour les tests
    ///////////////////////////////////////////////////////



    public ABR getFilsG() {
        return filsG;
    }

    public ABR getFilsD() {
        return filsD;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public void setFilsG(ABR filsG) {
        this.filsG = filsG;
    }

    public void setFilsD(ABR filsD) {
        this.filsD = filsD;
    }

    public static void main(String[] args){



    }

}
